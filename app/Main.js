import * as React from 'react';
import { Text, View, StyleSheet, StatusBar, ScrollView, ActivityIndicator, AsyncStorage, TouchableOpacity } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { MaterialIcons } from '@expo/vector-icons';
import uuid from 'uuid/v1';
import { primaryGradientArray, lightWhite } from './utils/Colors';
import Header from './components/Header';
import Input from './components/Input';
import List from './components/List';

const headerTitle = 'To Do';

export default class App extends React.Component {
  state = {
    inputValue: '',
    loadingItems: false,
    allItems: {},
    isComplete: false
  };

  componentDidMount = () => {
    this.loadingItems();
  }

  newInputValue = value => {
    this.setState({
      inputValue: value
    });
  };

  loadingItems = async () => {
    try {
      const allItems = await AsyncStorage.getItem('ToDos');
      this.setState({
        loadingItems: true,
        allItems: JSON.parse(allItems) || {}
      });
    } catch (err) {
      console.log(err);
    }
  };

  onDoneAddItem = () => {
    const { inputValue } = this.state;
    if (inputValue !== '') {
      this.setState(prevState => {
        const id = uuid();
        const newItemObject = {
          [id]: {
            id,
            isCompleted: false,
            text: inputValue,
            createdAt: Date.now()
          }
        };
        const newState = {
          ...prevState,
          inputValue: '',
          allItems: {
            ...prevState.allItems,
            ...newItemObject
          }
        };
        this.saveItems(newState.allItems);
        return { ...newState }
      });
    }
  };

  deleteItem = id => {
    this.setState(prevState => {
      const allItems = prevState.allItems;
      delete allItems[id];
      const newState = {
        ...prevState,
        ...allItems
      };
      this.saveItems(newState.allItems);
      return { ...newState };
    });
  };

  completeItem = id => {
    this.setState(prevState => {
      const newState = {
        ...prevState,
        allItems: {
          ...prevState.allItems,
          [id]: {
            ...prevState.allItems[id],
            isCompleted: true
          }
        }
      };
      this.saveItems(newState.allItems);
      return { ...newState };
    });
  };

  incompleteItem = id => {
    this.setState(prevState => {
      const newState = {
        ...prevState,
        allItems: {
          ...prevState.allItems,
          [id]: {
            ...prevState.allItems[id],
            isCompleted: false
          }
        }
      };
      this.saveItems(newState.allItems);
      return { ...newState };
    });
  };

  deleteAllItems = async () => {
    try {
      await AsyncStorage.removeItem('ToDos');
      this.setState({allItems: {}});
    } catch (err) {
      console.log(err);
    }
  };

  saveItems = newItem => {
    const saveItem = AsyncStorage.setItem('ToDos', JSON.stringify(newItem));
  };

  render() {
    const { inputValue, loadingItems, allItems } = this.state;
    return(
      <LinearGradient colors={primaryGradientArray} style={styles.container}>
        <StatusBar barStyle="light-content" />
        <View style={styles.centered}>
          <Header title={headerTitle} />
        </View>
        <View style={styles.inputContainer}>
          <Text style={styles.subtitle}>WHAT'S NEXT?</Text>
          <Input inputValue={inputValue} onChangeText={this.newInputValue} onDoneAddItem={this.onDoneAddItem} />
        </View>
        <View style={styles.list}>
          <View style={styles.column}>
            <Text style={styles.subtitle}>Recent Notes</Text>
            <View style={styles.deleteAllButton}>
              <TouchableOpacity onPressOut={this.deleteAllItems}>
                <MaterialIcons name="delete-sweep" size={24} color={lightWhite} />
              </TouchableOpacity>
            </View>
          </View>
          {loadingItems ? (
            <ScrollView contentContainerStyle={styles.scrollableList}>
              {Object.values(allItems).reverse().map(item => (
                <List key={item.id} {...item} deleteItem={this.deleteItem}
                  completeItem={this.completeItem} incompleteItem={this.incompleteItem} 
                />
              ))}
            </ScrollView>
          ) : (
                <ActivityIndicator size="large" color="white" />
              )
          }
        </View>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  centered: {
    alignItems: 'center'
  },
  inputContainer: {
    marginTop: 40,
    paddingLeft: 15
  },
  list: {
    flex: 1,
    marginTop: 70,
    paddingLeft: 15,
    marginBottom: 10
  },
  scrollableList: {
    marginTop: 15
  },
  subtitle: {
    color: 'white',
    fontSize: 20
  },
  column: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  deleteAllButton: {
    marginRight: 40
  },
});